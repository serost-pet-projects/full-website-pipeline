#!/bin/bash

mysql_requires_bootstrap=false

# https://stackoverflow.com/a/2875513
while test $# != 0; do
    case "$1" in
    -f) mysql_requires_bootstrap=true ;;
    esac
    shift
done

if [ ! -d .cache/mysql ] || [ "$mysql_requires_bootstrap" = "true" ]; then
    echo "MySQL volume not found, and will be bootstraped by ./scripts/bootstrap-mysql.sh"
    mysql_requires_bootstrap=true
fi

docker compose -f docker-compose.run.yaml up -d --force-recreate --build

if [ "$mysql_requires_bootstrap" = "true" ]; then
    MAX_RETRIES=5
    count=0
    retcode=1
    while [ $retcode -ne "0" ] && [ $count -lt $MAX_RETRIES ]; do
        sleep 1
        docker exec -i php-app-mysql \
            sh -c 'exec mysql -h 127.0.0.1 -uroot -ppassword123' <./sandbox/mysql/bootstrap.sql
        retcode=$?
        count=$(($count + 1))
    done
fi

echo done.
