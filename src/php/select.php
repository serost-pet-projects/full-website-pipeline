<?php

$db_endpoint = getenv('DB_ENDPOINT');
$db_username = getenv('DB_USERNAME');
$db_password = getenv('DB_PASSWORD');

# mysqli is good for MySQL databases, for other engines use PDO
$mysqli = new mysqli($db_endpoint, $db_username, $db_password, "php_app_db");

if ($mysqli->connect_errno) {
    exit();
}

$stmt = $mysqli->query("select * from tasks;");
$stmt_result = $stmt->fetch_all(MYSQLI_ASSOC);

foreach ($stmt_result as $row) {
    # NOTE: outputs are not sanitized here, since they are 
    # when we added them to a database in php/handle_form.php
    $row_name = $row['name'];
    $row_desc = $row['description'];

    echo '<div class = "selects">';
    echo '<h3>' . $row_name . "</h3>";
    echo '<br>';
    echo $row_desc;
    echo '<br>';
    echo '</div>';
    echo '<br>';
}

$mysqli->close();