<?php

header("Location: ../index.php");

if ($_SERVER['REQUEST_METHOD'] != "POST") {
    exit();
}

# cross site scripting is funny tho
$task_name = htmlspecialchars($_POST["task-name"]);
$task_desc = htmlspecialchars($_POST["task-desc"]);

if (
    empty($task_name) || strlen($task_name) >= 50 ||
    empty($task_desc) || strlen($task_desc) >= 200
) {
    exit();
}

$db_endpoint = getenv('DB_ENDPOINT');
$db_username = getenv('DB_USERNAME');
$db_password = getenv('DB_PASSWORD');

# mysqli is good for MySQL databases, for other engines use PDO
$mysqli = new mysqli($db_endpoint, $db_username, $db_password, "php_app_db");

if ($mysqli->connect_errno) {
    exit();
}

$stmt = $mysqli->prepare("insert into tasks (name, description) values (?, ?)");
$stmt->bind_param("ss", $task_name, $task_desc);
$stmt->execute();

$mysqli->close();