<!DOCTYPE html>
<html lang="en">

<!--
    head tag contains metadata about body (the actual web page),
    what to load, title, what charset to use, etc.
-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Task Website!</title>
    <link rel="stylesheet" href="reset.css">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <!--
        At this case form will tell browser to send get request to php file:
        GET /php/handle_form.php?task-name=Take+out+trash...
    -->
    <form class="task-form-input" action="php/handle_form.php" method="post">
        <label for="task-name">Task name:</label>
        <br>
        <!-- 
            "name" is what passed as a key in http request
            (either get or post depending on form method)

            whereas "id" is unique identifier within the file,
            that is used to apply labels and whatever.
        -->
        <input type="text" id="task-name" name="task-name" placeholder="Enter task name:">
        <br>
        <label for="task-desc">Task description:</label>
        <br>
        <textarea id="task-desc" name="task-desc" placeholder="Enter task description:"></textarea>
        <br>
        <button class="task-form-input-button-submit" onclick="submit">Submit!</button>
    </form>

    <br>

    <?php
    # show what is already in a database
    require("php/select.php");
    ?>

</body>

</html>