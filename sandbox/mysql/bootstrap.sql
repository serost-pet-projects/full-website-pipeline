# username@host are combined as a user unit.
create user if not exists 'php_app' @'%' identified with mysql_native_password by 'php-password123';

create database if not exists php_app_db;

create table if not exists php_app_db.tasks(
    id int primary key auto_increment,
    name varchar(50),
    description varchar(200)
);

grant
select
    on php_app_db.tasks to 'php_app' @'%';

grant
insert
    on php_app_db.tasks to 'php_app' @'%';

# if needed grant update later.
# something like:
# grand update(description) on php_app_db.tasks to 'php_app'@'%';